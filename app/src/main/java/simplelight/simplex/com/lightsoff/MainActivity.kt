package simplelight.simplex.com.lightsoff

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.PendingIntent
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatButton
import com.shawnlin.numberpicker.NumberPicker

class MainActivity : AppCompatActivity() {
    private lateinit var np: NumberPicker
    private var ACTIVATION_REQUEST = 17
    private lateinit var adminReciever: ComponentName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var sharedPref = getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
        var editor = sharedPref.edit()
        editor.putInt(SAVED_TIME, 5).commit()


        np = findViewById(R.id.number_picker)
        adminReciever = ComponentName(this, AdminReceiver::class.java)

        var button = findViewById<AppCompatButton>(R.id.button)
        button.setOnClickListener {
            var thread = Thread(Runnable {
                checkPermissions()
                var intent = Intent(applicationContext, LightsService::class.java)
                intent.putExtra("time", (np.value).toLong())
                println(np.value)
                var sharedPref = getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
                var editor = sharedPref.edit()
                editor.putInt(SAVED_TIME, np.value).commit()
                startService(intent)
            })
            thread.start()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun checkPermissions() {
        println("Checking permissions")
        /*if(np.value == 1) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                println("Good version")
                if (!Settings.System.canWrite(this)) {
                    println("Starting permission intent")
                    var intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
                    startActivity(intent)
                }
            }
        } else if(np.value == 2) {*/
            var sharedPref = getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
            var adminStatus = sharedPref.getBoolean(ADMIN_STATUS, false)
            if(!adminStatus) {
                println("Starting device admin intent")
                var intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminReciever)
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                        "Sample Explanation")
                startActivityForResult(intent, ACTIVATION_REQUEST)
            }

        //}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ACTIVATION_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    println("Enabled")
                }
            }
        }
    }
}
