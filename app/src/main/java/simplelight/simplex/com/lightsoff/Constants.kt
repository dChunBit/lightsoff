package simplelight.simplex.com.lightsoff

/**
 * Created by dwsch on 7/30/2017.
 */

var DEVICE_ADMIN = "device_admin"
var ADMIN_STATUS = "admin_status"
var SAVED_TIME = "saved_time"

enum class ServiceParams {
    START_FOREGROUND, STOP_FOREGROUND, STOP_SERVICE
}