package simplelight.simplex.com.lightsoff

import android.app.admin.DevicePolicyManager
import android.content.Context

/**
 * Created by dwsch on 7/30/2017.
 */
class ScreenRunnable constructor(context_from: Context): Runnable {
    var context = context_from

    override fun run() {
        var sharedPref = context.getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
        var adminStatus = sharedPref.getBoolean(ADMIN_STATUS, false)
        if(adminStatus) {
            val devicePolicyManager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
            devicePolicyManager.lockNow()
        }
    }
}