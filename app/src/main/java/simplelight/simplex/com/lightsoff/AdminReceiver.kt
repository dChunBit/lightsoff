package simplelight.simplex.com.lightsoff

import android.app.admin.DeviceAdminReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast





/**
 * Created by dwsch on 7/30/2017.
 */
class AdminReceiver: DeviceAdminReceiver() {
    fun showToast(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onEnabled(context: Context, intent: Intent) {
        println("ENABLED")
        var sharedPref = context.getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
        var editor = sharedPref.edit()
        editor.putBoolean(ADMIN_STATUS, true).commit()
    }

    override fun onDisableRequested(context: Context, intent: Intent): CharSequence {
        return "DISABLE WARNING"
    }

    override fun onDisabled(context: Context, intent: Intent) {
        showToast(context, "DISABLED")
        var sharedPref = context.getSharedPreferences(DEVICE_ADMIN, Context.MODE_PRIVATE)
        var editor = sharedPref.edit()
        editor.putBoolean(ADMIN_STATUS, false).commit()
    }
}